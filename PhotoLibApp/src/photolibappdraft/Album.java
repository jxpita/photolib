/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package photolibappdraft;

import java.util.ArrayList;

/**
 *
 * @author usuario
 */
public class Album {
    
    private String albumName;
    private String albumOverview;
    private String albumPath;

    public Album() {
        
    }
    
    public Album(String albumName, String albumOverview, String albumPath) {
        this.albumName = albumName;
        this.albumOverview = albumOverview;
        this.albumPath = albumPath;
    }
    

    public String getAlbumName() {
        return albumName;
    }

    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }

    public String getAlbumOverview() {
        return albumOverview;
    }

    public void setAlbumOverview(String albumOverview) {
        this.albumOverview = albumOverview;
    }

    public String getAlbumPath() {
        return albumPath;
    }

    public void setAlbumPath(String albumPath) {
        this.albumPath = albumPath;
    }

    @Override
    public String toString() {
        return "Album{" + "albumName=" + albumName + ", albumOverview=" + albumOverview + ", albumPath=" + albumPath + '}';
    }
}
