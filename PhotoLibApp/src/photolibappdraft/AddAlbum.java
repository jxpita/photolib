/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package photolibappdraft;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 *
 * @author usuario
 */
public class AddAlbum {

    private BorderPane AddAlbumDataWindow;
    private HBox bottom;
    private GridPane center;
    private VBox upper;
    private TextField textFieldNombre;
    private TextArea textAreaDescripcion;
    private Button salirAlbum;
    private Button grabarAlbum;
    
    public AddAlbum() {
        bottom = new HBox();
        AddAlbumDataWindow = new BorderPane();
        upper = new VBox();
        center = new GridPane();
        textFieldNombre = new TextField();
        textAreaDescripcion = new TextArea();
        
        
        center.setPadding(new Insets(5));
        center.setHgap(5);
        center.setVgap(5);
        ColumnConstraints column1 = new ColumnConstraints(100);
        ColumnConstraints column2 = new ColumnConstraints(50, 150, 300);
        column2.setHgrow(Priority.ALWAYS);
        center.getColumnConstraints().addAll(column1, column2);
        
        Label titulo= new Label("INGRESO DE DATOS ALBUM");
        Label labelDescripcion= new Label("Nombre: ");
        Label labelLugar= new Label("Descripción: ");
        Button limpiar= new Button("Limpiar");
        grabarAlbum = new Button ("Guardar");
        salirAlbum = new Button("Salir");
        
        
        GridPane.setHalignment(labelDescripcion, HPos.CENTER);
        center.add(labelDescripcion, 0, 0);
        GridPane.setHalignment(textFieldNombre, HPos.RIGHT);
        center.add(textFieldNombre, 1, 0);
        
        
        GridPane.setHalignment(labelLugar, HPos.CENTER);
        center.add(labelLugar, 0, 1);
        GridPane.setHalignment(textAreaDescripcion, HPos.RIGHT);
        center.add(textAreaDescripcion, 1, 1);
        
        
        upper.getChildren().add(titulo);
        upper.setAlignment(Pos.CENTER);
        bottom.getChildren().addAll(limpiar,grabarAlbum,salirAlbum);
        bottom.setAlignment(Pos.CENTER);
        bottom.setSpacing(10);
        
        
        AddAlbumDataWindow.setCenter(center);
        AddAlbumDataWindow.setTop(upper);
        AddAlbumDataWindow.setBottom(bottom);
        
        limpiar.setOnAction(new Limpiar());
        
    }
    
    private class Limpiar implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent event) {
            textFieldNombre.setText("");
            textAreaDescripcion.setText("");
        }

    }

    

    public Button getGrabarAlbum() {
        return grabarAlbum;
    }
    
    public Button getSalirAlbum() {
        return salirAlbum;
    }

    public BorderPane getAddAlbumDataWindow() {
        return AddAlbumDataWindow;
    }

    public TextField getTextFieldNombre() {
        return textFieldNombre;
    }

    public TextArea getTextAreaDescripcion() {
        return textAreaDescripcion;
    }
}
