
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package photolibappdraft;

import javafx.scene.control.Label;
import javafx.scene.paint.Color;

/**
 *
 * @author usuario
 */
public class StyledLabel {
    private Label label;
    
    public StyledLabel(){
        label = new Label();
    }
    
    public StyledLabel(String nombre, String estilo, double ancho, double alto, double posX, double posY, Color color, boolean sombra) {
        label = new Label(nombre);
        label.setPrefSize(ancho, alto);
        label.setLayoutX(posX);
        label.setLayoutY(posY);
        label.setStyle(estilo);
        label.setTextFill(color);
    }

    public Label getLabel() {
        return label;
    }

    public void setLabel(Label label) {
        this.label = label;
    }
    
}
