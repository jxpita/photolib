/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package photolibappdraft;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author usuario
 */
public class PhotoLibAppDraft extends Application {
    
//    @Override
    public void start(Stage primaryStage) {
        
        Gallery gallery = new Gallery(primaryStage);
        
        Scene scene = new Scene(gallery.getRoot(), 900, 800);
        
        primaryStage.setTitle("Photo Library Application");
        primaryStage.setScene(scene);
        primaryStage.show();
        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
