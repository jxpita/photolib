/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package photolibappdraft;

import java.io.File;
import java.util.ArrayList;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

/**
 *
 * @author usuario
 */
public class SlideShowWindow implements Runnable{

    private BorderPane slideShowWindow;
    private HBox bottom;
    private Button play;
    private Button pausar;
    private Slider slider;
    private ImageView imagenActual;
    boolean flag;
    int tiempoEspera = 5;
    Thread hilo = null;
    
    public SlideShowWindow() {
        bottom = new HBox();
        slideShowWindow = new BorderPane();
        imagenActual = new ImageView();
        slider = new Slider();
        
        System.out.println(imagenActual.getFitHeight());
        System.out.println(imagenActual.getFitWidth());
        
        slider.setMin(0);
        slider.setMax(15);
        slider.setValue(5);
        slider.setShowTickLabels(true);
        slider.setShowTickMarks(true);
        slider.setMajorTickUnit(1); //cuantas unidades quieres que salga 
        slider.setMinorTickCount(1); //cuantas barritas sale entre numeros
        slider.setBlockIncrement(1); //cuanto se mueve con las direccionales  
        
        
        play= new Button("Play");
        pausar = new Button ("pausar");
        
        
        slideShowWindow.setCenter(imagenActual);
        
        bottom.getChildren().addAll(play,pausar, slider);
        bottom.setAlignment(Pos.CENTER);
        bottom.setSpacing(10);
        
        
        slideShowWindow.setBottom(bottom);
        
        play.setOnAction(eventPlay ->{ 
            if (hilo == null){
                        hilo = new Thread(this);
                        hilo.start();
                    }else if (hilo.getState().toString().equals("TERMINATED")){
                        flag = true;
                        hilo = new Thread(this);
                        hilo.start();
                    }else if (hilo.getState().toString().equals("TIMED_WAITING")){
                        hilo.resume();
                    }
        });
        
        pausar.setOnAction(eventStop ->{
            try{
                hilo.suspend();
                System.out.println(hilo.getState().toString());
            }catch(Exception ex){
                ex.printStackTrace();
            }
            
        });
        
        
        
        
    }

    @Override
    public void run() {
            int segundos = -1;
            while(flag){               
                String path = "C:\\Users\\usuario\\Desktop\\Nueva carpeta\\PhotoLibAppDraft\\src\\setPhotos";
                System.out.println(Scroller.currentPath);
                
                ArrayList<Image> listFoto = this.getImages(System.getProperty("user.dir") + "\\" + Scroller.currentPath);
                for (Image im : listFoto){
                    try {
                        imagenActual.setImage(im);
                        tiempoEspera = (int)slider.getValue();
                        imagenActual.setFitHeight(900);
                        imagenActual.setFitWidth(800);
                        imagenActual.setPreserveRatio(true);
                        Thread.sleep(tiempoEspera*1000);
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }                           
            }                        
    }

    public BorderPane getSlideShowWindow() {
        return slideShowWindow;
    }
    
    public ArrayList<Image> getImages(String folderPath){
        ArrayList<Image> images = new ArrayList<>();
        File folder = new File(folderPath);
        if (folder.isDirectory()) {
            for (File file:folder.listFiles()) {
                if (file.isFile() && !file.isHidden()) {
                    images.add(new Image("file:\\" + file.getAbsolutePath()));
                }                
            }
        } else {
            System.out.println("Ruta no es un directorio valido");
            return null;
        }        
        return images;
    }

    public void stop(){
        flag = false;        
        System.out.println(this.flag +" "+ this.hashCode());
    }
}