/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package photolibappdraft;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javafx.scene.control.Dialog;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.util.Pair;

/**
 *
 * @author usuario
 */
public class Gallery implements Serializable{
    
    
    private BorderPane root;
    private GalleryToolBar galleryToolbar;
    private Scroller scrollerCenterNode;
    

    public Gallery(Stage stage) {
        root = new BorderPane();
        galleryToolbar = new GalleryToolBar(stage);
        
        scrollerCenterNode = new Scroller(stage, galleryToolbar.getBack().getBtn());        

        root.setTop(galleryToolbar.getGalleryToolbar());
        root.setCenter(scrollerCenterNode.getScroller());
    }
    
    public static void serializar(GalleryToolBar gallTool){
        try {
            FileOutputStream fout = new FileOutputStream("infoGalleryt.txt");
            ObjectOutputStream  out = new ObjectOutputStream(fout);
            out.writeObject(gallTool);  
            out.flush();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    
    public static void deseralizar(GalleryToolBar gallTool){
        ObjectInputStream in=null;
        try {
            in = new ObjectInputStream(new FileInputStream("f.txt"));
            gallTool = (GalleryToolBar)in.readObject();
            in.close();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException | ClassNotFoundException ex) {
            ex.printStackTrace();
        } 
    }
    
    public void mostrarCuadroDialogoNuevaFotografia(){
        Dialog<Pair<String, String>> dialog = new Dialog<>();
        dialog.setTitle("Nueva Fotografia");
        dialog.setHeaderText("Nueva Fotografía");    
    }

    public BorderPane getRoot() {
        return root;
    }

    public void setRoot(BorderPane root) {
        this.root = root;
    }    
    
       
   }
