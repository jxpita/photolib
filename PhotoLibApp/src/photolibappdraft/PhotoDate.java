/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package photolibappdraft;

/**
 *
 * @author usuario
 */
public class PhotoDate {

    private int dia;
    private String mes;
    private int anio;

    public PhotoDate(){
        this.dia = 0;
        this.mes = "";
        this.anio = 0;
    }

    public PhotoDate(int dia, String mes, int anio) {
        this.dia = dia;
        this.mes = mes;
        this.anio = anio;
    }
    
    

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    

    public int getAño() {
        return anio;
    }

    public void setAño(int año) {
        this.anio = año;
    }

    @Override
    public String toString() {
        return dia + "-" + mes + "-" + anio;
    }
    
} 
