/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package photolibappdraft;

import java.util.List;

/**
 *
 * @author usuario
 */
public class Photography {
    private String description;
    private String location;
    private List<String> persons;
    private String albumName;
    private PhotoDate date;
    private String photoPath;
    
    public Photography(){
    
    }

    public Photography(String description, String location, List<String> persons, String albumName, PhotoDate date, String photoPath) {
        this.description = description;
        this.location = location;
        this.persons = persons;
        this.albumName = albumName;
        this.date = date;
        this.photoPath = photoPath;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public List<String> getPersons() {
        return persons;
    }

    public void setPersons(List<String> persons) {
        this.persons = persons;
    }

    public String getAlbumName() {
        return albumName;
    }

    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }

    public PhotoDate getDate() {
        return date;
    }

    public void setDate(PhotoDate date) {
        this.date = date;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    @Override
    public String toString() {
        return "Photography{" + "description=" + description + ", location=" + location + ", persons=" + persons + ", albumName=" + albumName + ", date=" + date + ", photoPath=" + photoPath + '}';
    }

    
}
