/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package photolibappdraft;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 *
 * @author usuario
 */
public class AddPhotoData {
    private BorderPane AddPhotoDataWindow;
    private HBox bottom;
    private GridPane center;
    private VBox upper;
    private TextField textFieldLugar;
    private TextArea descripcionBox;
    private DatePicker fecha;    
    private TextField textFieldPersona;
    private ListView listViewPersona;  
    private Button grabarPhotoData;
    private Button salirPhotoData;
    
    
    public AddPhotoData() {
        bottom = new HBox();
        AddPhotoDataWindow = new BorderPane();
        upper = new VBox();
        center = new GridPane();
        fecha = new DatePicker();
        textFieldLugar = new TextField();
        descripcionBox = new TextArea();
        textFieldPersona = new TextField();
        listViewPersona = new ListView();
      
        ObservableList<String> listadoPersonas = FXCollections.observableArrayList ();
        listViewPersona.setItems(listadoPersonas);
        
        center.setPadding(new Insets(5));
        center.setHgap(5);
        center.setVgap(5);
        ColumnConstraints column1 = new ColumnConstraints(100);
        ColumnConstraints column2 = new ColumnConstraints(50, 150, 300);
        column2.setHgrow(Priority.ALWAYS);
        center.getColumnConstraints().addAll(column1, column2);
        
        Label titulo= new Label("INGRESO DE DATOS DE FOTOGRAFIA");
        Label labelDescripcion= new Label("Descripción:");
        Label labelLugar= new Label("Lugar:");
        Label labelFecha= new Label("Fecha:");
        Label labelPersona = new Label("Persona:");
        Button limpiar= new Button("Limpiar");
        grabarPhotoData = new Button ("Grabar");
        salirPhotoData = new Button("Salir");        
                
        GridPane.setHalignment(labelDescripcion, HPos.CENTER);
        center.add(labelDescripcion, 0, 0);
        GridPane.setHalignment(descripcionBox, HPos.RIGHT);
        center.add(descripcionBox, 1, 0);        
        
        GridPane.setHalignment(labelLugar, HPos.CENTER);
        center.add(labelLugar, 0, 1);
        GridPane.setHalignment(textFieldLugar, HPos.RIGHT);
        center.add(textFieldLugar, 1, 1);
        
        GridPane.setHalignment(labelFecha, HPos.CENTER);
        center.add(labelFecha, 0, 2);
        GridPane.setHalignment(fecha, HPos.LEFT);
        center.add(fecha, 1,2);

        GridPane.setHalignment(labelPersona, HPos.CENTER);
        center.add(labelPersona, 0, 4);
        GridPane.setHalignment(textFieldPersona, HPos.LEFT);
        center.add(textFieldPersona, 1, 4);
        
        GridPane.setHalignment(listViewPersona, HPos.LEFT);
        center.add(listViewPersona, 1, 5);
        
        listViewPersona.getItems();
        textFieldPersona.setOnKeyPressed(e -> {
            if (!(textFieldPersona.getText().isEmpty())){
                if((e.getCode().equals(KeyCode.ENTER)) &&  !(listadoPersonas.contains(textFieldPersona.getText()))){
                    listadoPersonas.add(textFieldPersona.getText());
                    listViewPersona.setItems(listadoPersonas);
                    textFieldPersona.setText("");
                }
            }
        });        
        
        listViewPersona.setOnKeyPressed(e->{
            if(!(listViewPersona.getItems().isEmpty())){
                 if((e.getCode().equals(KeyCode.DELETE)) || (e.getCode().equals(KeyCode.BACK_SPACE))){
                     listViewPersona.getItems().remove(listViewPersona.getSelectionModel().getSelectedItem());
                 }
            }
        });
        
        upper.getChildren().add(titulo);
        upper.setAlignment(Pos.CENTER);
        bottom.getChildren().addAll(limpiar,grabarPhotoData,salirPhotoData);
        bottom.setAlignment(Pos.CENTER);
        bottom.setSpacing(10);
        
        
        AddPhotoDataWindow.setCenter(center);
        AddPhotoDataWindow.setTop(upper);
        AddPhotoDataWindow.setBottom(bottom);
        
        limpiar.setOnAction(new Limpiar());
        
    }
    
    private class Limpiar implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent event) {
            descripcionBox.setText("");
            textFieldLugar.setText("");
            fecha.getEditor().clear();
            textFieldPersona.setText("");
            listViewPersona.getItems().clear();
        }

    }

    public TextArea getDescripcionBox() {
        return descripcionBox;
    }    
    
    public TextField getTextFieldLugar() {
        return textFieldLugar;
    }
    
    public DatePicker getFecha() {
        return fecha;
    }
    
    public ListView getListViewPersona() {
        return listViewPersona;
    }
    
    public boolean verificarCamposVacios(){
        return descripcionBox.getText().isEmpty() || textFieldLugar.getText().isEmpty() || fecha.getValue() == null; 
    }

    public Button getGrabarPhotoData() {
        return grabarPhotoData;
    }
    
    public Button getSalirPhotoData() {
        return salirPhotoData;
    }

    public BorderPane getAddPhotoDataWindow() {
        return AddPhotoDataWindow;
    }
}
