/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package photolibappdraft;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 *
 * @author usuario
 */
public class Scroller extends Thread {
    
    private Stage stage;    
    private ScrollPane scroller;
    static String currentPath;
    private String lastPath;
    private Button buttonBack;
    final String GALLERY_PATH = "/src/setPhotos";
    private int counter;
    private int counterBack;
    

    public Scroller(Stage primaryStage, Button btnBack) {
        stage = primaryStage;
        scroller = new ScrollPane();
        scroller.setStyle("-fx-background-color: DAE6F3;");
        
        buttonBack = btnBack;
        
        buttonBack.setVisible(false);
        buttonBack.setDisable(false);
        
        
        scroller.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER); // Horizontal
        scroller.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED); // Vertical scroll bar
        scroller.setFitToWidth(true);
        currentPath = "\\src\\setPhotos";
        scroller.setContent(displayContent(currentPath));
        counter = 0;
        counterBack = 0;

        this.buttonBack.setOnAction(e -> {
            if (counter != 0) {
                
                counter--;      // USAR CONTADOR PARA SABER CUANDO GIRARLO Y CUANDO NO
                counterBack++; 
                if (counterBack >= 2) {
                    this.lastPath = this.currentPath;    
                    System.out.println(this.lastPath);
                    System.out.println(this.currentPath);
                    this.currentPath = String.join("\\", slicePathArray(this.lastPath.split("\\\\")));
                    scroller.setContent(this.displayContent(currentPath));
                    counterBack--;
                    System.out.println(this.currentPath);
                    
                } else {
                    String temp = this.lastPath;
                    this.lastPath = this.currentPath;                
                    this.currentPath = String.join("\\", slicePathArray(temp.split("\\\\")));    
                    scroller.setContent(this.displayContent(currentPath));                                   
                }
                System.out.println(counter);
                System.out.println("counter back:\t" + counterBack);
                
                System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            } else {
                scroller.setContent(this.displayContent(GALLERY_PATH));
                counterBack = 0;
            }
            System.out.println("Ultimo: \t"   + this.lastPath);
            System.out.println("Actual: \t" + this.currentPath);
            System.out.println("````````````````````````````");
                            
        });
    }
    
    public ArrayList slicePathArray(String[] pathArray) {
        ArrayList<String> folders = new ArrayList<>(Arrays.asList(pathArray));
        ArrayList<String> pathArrayList = new ArrayList<>();
        int boundary = 1;
        if (counterBack >= 2) {
            boundary = 2;
        }
        for (int i = 0; i <= folders.size()-boundary; i++) {
            pathArrayList.add(folders.get(i));
        }
        return pathArrayList;
    }
    
    public boolean comparePaths(String currentPath){
        if (currentPath.contains(":")) {
            return currentPath.split(":")[1].equals(GALLERY_PATH);
        } else {
            return currentPath.equals(GALLERY_PATH);
        }        
    }
    
    public TilePane displayContent(String currentAbsolutePath) {
        TilePane tile = new TilePane();
        tile.setPadding(new Insets(15, 15, 15, 15));
        tile.setHgap(15);

        File folderAbsolutePath = new File(currentAbsolutePath);
        // Obtener el path absoluto de la carpeta actual
        String folderRelativePath = new File(System.getProperty("user.dir")).toURI().relativize(folderAbsolutePath.toURI()).getPath();

        if(!this.comparePaths(folderRelativePath)) {
            this.buttonBack.setVisible(true);
            this.buttonBack.setDisable(false);
        } else {
            this.buttonBack.setVisible(false);
            this.buttonBack.setDisable(true);
        }

        String path = folderAbsolutePath.toString();
        if  (!folderAbsolutePath.toString().contains(".\\")) {
            path = ".\\" + path;
        } 
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();

        for (final File file : listOfFiles) {
            VBox fileBox = new VBox();
            fileBox.setAlignment(Pos.CENTER);
            fileBox.setSpacing(0);
            fileBox.setMaxSize(50, 75);     // CORREGIR DISPLAY DE LABEL
            if (!file.isHidden()) {
                if (file.isFile()) {
                    ImageView imageView = createImageView(file);
                    fileBox.getChildren().add(imageView);
                    Label label = new Label(getFileNameWithoutExtension(file));
                    label.setWrapText(true);
                    label.setAlignment(Pos.CENTER);
                    fileBox.getChildren().add(label);
                    tile.getChildren().add(fileBox);
                } else if (file.isDirectory()) {    // SET LABEL TO CENTER!!                    
                    ImageView folderImage = createFolderView(file.getPath());
                    Label label = new Label(file.getName());
                    label.setWrapText(true);
                    fileBox.getChildren().addAll(folderImage, label);
                    tile.getChildren().add(fileBox);
                }
            }
        }
        return tile;
    }
    
    public String getFileNameWithoutExtension(File file) {
        String fileName = ""; 
        String name = file.getName();
        fileName = name.replaceFirst("[.][^.]+$", "");
        return fileName;
 
    }
    
    public ImageView createFolderView(String currentAbsolutePath) {
        ImageView imageView = null;       
        try {
            final Image image = new Image(new FileInputStream("src/folder.png"), 150, 0, true, true);
            imageView = new ImageView(image);
            imageView.setFitWidth(150);
            imageView.setOnMouseClicked((MouseEvent mouseEvent) -> {
                if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
                    if (mouseEvent.getClickCount() == 2) {
                        try {
                            scroller.setContent(this.displayContent(currentAbsolutePath));
                            this.counter++;                            
                            System.out.println(counter);
                            this.lastPath = this.currentPath;
                            this.currentPath = currentAbsolutePath;
                            System.out.println("Ultimo: \t"   + this.lastPath);
                            System.out.println("Actual: \t" + this.currentPath);
                            System.out.println("_________________");
                            
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Scroller.class.getName()).log(Level.SEVERE, null, ex);
        }
        return imageView;
    }
    
    public ImageView createImageView(final File imageFile) {
        ImageView imageView = null;
        try {
            final Image image = new Image(new FileInputStream(imageFile), 150, 0, true, true);
            imageView = new ImageView(image);
            imageView.setFitWidth(150);
            imageView.setOnMouseClicked((MouseEvent mouseEvent) -> {
                if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
                    if (mouseEvent.getClickCount() == 2) {
                        try {
                            BorderPane borderPane = new BorderPane();
                            ImageView imageView1 = new ImageView();
                            Image image1 = new Image(new FileInputStream(imageFile));
                            imageView1.setImage(image1);
                            imageView1.setStyle("-fx-background-color: BLACK");
                            imageView1.setFitHeight(stage.getHeight() - 10);
                            imageView1.setPreserveRatio(true);
                            imageView1.setSmooth(true);
                            imageView1.setCache(true);
                            borderPane.setCenter(imageView1);
                            borderPane.setStyle("-fx-background-color: BLACK");
                            Stage newStage = new Stage();
                            newStage.setWidth(stage.getWidth());
                            newStage.setHeight(stage.getHeight());
                            newStage.setTitle(imageFile.getName());
                            Scene scene = new Scene(borderPane,Color.BLACK);
                            newStage.setScene(scene);
                            newStage.show();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        return imageView;
    }
    
    public ScrollPane getScroller() {
        return scroller;
    }

}
