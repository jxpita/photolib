package photolibappdraft;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.Scene;
import javafx.scene.control.ToolBar;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author usuario
 */
public class GalleryToolBar implements Serializable{
    
    private ToolBar galleryToolbar;
    private StyledButton addPhoto;
    private StyledButton createAlbum;
    private StyledButton slideshowMode;
    private StyledButton back;
    private FileChooser fileChooserPhoto;
    private Stage tempStage;
    private Path targetPath;
    final String GALLERY_PATH = "/src/setPhotos";
    private ArrayList<Object> infoGallery; // Contiene albums y fotos.
    
    public GalleryToolBar(Stage stage) {
        infoGallery= new ArrayList<Object>();
        addPhoto = new StyledButton("Añadir Nueva Foto", "-fx-font: 20 century; -fx-background-radius: 30;", 200,100, 0, 0, 5, Color.BROWN);
        createAlbum = new StyledButton("Crear Álbum", "-fx-font: 20 century; -fx-background-radius: 30;", 200,100, 0, 0, 5, Color.BROWN);
        slideshowMode = new StyledButton("Modo SlideShow", "-fx-font: 20 century; -fx-background-radius: 30;", 200,100, 0, 0, 5, Color.BROWN);
        back = new StyledButton("Atrás", "-fx-font: 20 century; -fx-background-radius: 30;", 200,100, 0, 0, 5, Color.BROWN);
        galleryToolbar = new ToolBar(
            addPhoto.getBtn(), 
            createAlbum.getBtn(), 
            slideshowMode.getBtn(),
            back.getBtn()
        );
        
        // BOTON AÑADIR FOTO
        fileChooserPhoto = new FileChooser();
        addPhoto.getBtn().setOnAction(eventAddPhoto -> {
            FileChooser.ExtensionFilter imageFilter= new FileChooser.ExtensionFilter("Image Files", "*.jpg", "*.png");
            fileChooserPhoto.getExtensionFilters().add(imageFilter);
            File selectedFile = fileChooserPhoto.showOpenDialog(stage);
            System.out.println("--------------------------------------------------");   // BORRAR                    

            // CUANDO SE HACE CLICK EW SALIR/X (boton X) EN VENTANA DE DATOS -> IGNORAR TODA ACCIÒN [checkeado]
            if(selectedFile != null){
                //SE ABRIRA UN CUADRO DE DIALOGO PARA ASIGNAR LA INFO DE LA FOTOGRAFIA
                AddPhotoData dataPhoto = new AddPhotoData();
                tempStage = new Stage();
                tempStage.setTitle("Ingreso de Datos");
                Scene scene = new Scene(dataPhoto.getAddPhotoDataWindow(), 330, 300, Color.BLACK);
                tempStage.setScene(scene);
                tempStage.show();
                dataPhoto.getSalirPhotoData().setOnAction(ev -> {
                    tempStage.close();
                });
                // BOTON GRABAR FOTO
                dataPhoto.getGrabarPhotoData().setOnAction(eventGrabarPhoto -> {
                    if(!dataPhoto.verificarCamposVacios()){
                        String descripcion = dataPhoto.getDescripcionBox().getText();
                        String location = dataPhoto.getTextFieldLugar().getText();
                        List<String> persons = dataPhoto.getListViewPersona().getItems();
                        String albumName = Scroller.currentPath.split("\\\\")[Scroller.currentPath.split("\\\\").length-1];
                        PhotoDate date = new PhotoDate(dataPhoto.getFecha().getValue().getDayOfMonth(),
                                dataPhoto.getFecha().getValue().getMonth().toString().toLowerCase(),
                                dataPhoto.getFecha().getValue().getYear());
                        infoGallery.add(new Photography(descripcion,
                                                                location,
                                                                persons,
                                                                albumName,
                                                                date,
                                                                selectedFile.getName()));
                        for (Object o: infoGallery) {
                            System.out.println(o.toString());
                        }
                        if(tempStage.isShowing()){
                            System.out.println("--------------------------------------------------");
                            
                            String relativeTargetPath = "." + Scroller.currentPath + "\\";     //path mutable??
                            targetPath = Paths.get(relativeTargetPath + selectedFile.getName());
                            System.out.println(targetPath);
                            try {
                                Files.copy(selectedFile.toPath(), targetPath);
                            } catch (IOException ex) {
                                Logger.getLogger(GalleryToolBar.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            
                            System.out.println("Archivo añadido satisfactoriamente");
                        }
                        tempStage.close();
                    }
                });
            } else {
                System.out.println("el archivo no fue seleccionado");
            }        
        });
        
        // BOTON CREAR ALBUM
        createAlbum.getBtn().setOnAction(eventAlbum ->{
            AddAlbum dataAlbum= new AddAlbum();
            tempStage = new Stage();
            tempStage.setTitle("Ingreso de Datos");
            Scene scene = new Scene(dataAlbum.getAddAlbumDataWindow(), 330, 300, Color.BLACK);
            tempStage.setScene(scene);

            tempStage.show();
            dataAlbum.getSalirAlbum().setOnAction(ev -> {                        
                tempStage.close();
            });
            
            // BOTON GRABAR ALBUM
            dataAlbum.getGrabarAlbum().setOnAction(eventGrabarAlbum ->{ 
                if(!dataAlbum.getTextFieldNombre().getText().isEmpty() && 
                   !dataAlbum.getTextAreaDescripcion().getText().isEmpty()) {
                    infoGallery.add(new Album(dataAlbum.getTextFieldNombre().getText(), 
                                                     dataAlbum.getTextAreaDescripcion().getText(),
                                                     Scroller.currentPath + "\\" + dataAlbum.getTextFieldNombre().getText()));
                    tempStage.close();
                    File newFolder = new File(System.getProperty("user.dir") + "\\" + Scroller.currentPath + "\\" 
                            + dataAlbum.getTextFieldNombre().getText());
                    try {
                        Files.createDirectories(Paths.get(newFolder.toString())); 
//                        Gallery.serializar(this);
                        System.out.println(newFolder);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            });
        });
        
        // BOTON SLIDESHOW
        slideshowMode.getBtn().setOnAction(eventSlideShow ->{
            SlideShowWindow ssw = new SlideShowWindow();
            tempStage = new Stage();
            tempStage.setTitle("Modo SlideShow");
            Scene scene = new Scene(ssw.getSlideShowWindow(), 330, 300, Color.BLACK);
            tempStage.setScene(scene);
            
            tempStage.setFullScreen(true);
            tempStage.show();
        
        });
    }

    public GalleryToolBar(ToolBar galleryToolbar, StyledButton addPhoto, StyledButton createAlbum, StyledButton addPersons, StyledButton slideshowMode) {
        this.galleryToolbar = galleryToolbar;
        this.addPhoto = addPhoto;
        this.createAlbum = createAlbum;
        this.slideshowMode = slideshowMode;
    }
    
    

    public ToolBar getGalleryToolbar() {
        return galleryToolbar;
    }

    public StyledButton getAddPhoto() {
        return addPhoto;
    }

    public StyledButton getCreateAlbum() {
        return createAlbum;
    }


    public StyledButton getSlideshowMode() {
        return slideshowMode;
    }

    public StyledButton getBack() {
        return back;
    }
}
